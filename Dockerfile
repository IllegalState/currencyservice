FROM java:8-jre
ADD /build/libs/currency-service-1.0.0.jar currency-service-1.0.0.jar
ENTRYPOINT ["java","-jar","currency-service-1.0.0.jar"]
