/**
 * Web Service provides exchange rates of the euro against foreign currencies, published by the European Central Bank and the Bank of Lithuania.
 * 
 */
@javax.xml.bind.annotation.XmlSchema(namespace = "http://www.lb.lt/WebServices/FxRates", elementFormDefault = javax.xml.bind.annotation.XmlNsForm.QUALIFIED)
package lb.client;
