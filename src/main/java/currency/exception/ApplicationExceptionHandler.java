package currency.exception;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.logging.Logger;

@ControllerAdvice
public class ApplicationExceptionHandler extends ResponseEntityExceptionHandler {

    private static final Logger log = Logger.getLogger(ApplicationExceptionHandler.class.getName());

    @ExceptionHandler(value = ApplicationException.class)
    protected ResponseEntity<Object> handleConflict(ApplicationException e, WebRequest request) {
        Exception cause = e.getCause();
        String body = cause.getMessage();

        log.warning("Failed: " + body + "\n" + cause.getStackTrace());

        return handleExceptionInternal(e, body, new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR, request);
    }

}
