package currency.exception;

public class ApplicationException extends RuntimeException {

    private Exception cause;

    public ApplicationException(Exception cause) {
        this.cause = cause;
    }

    @Override
    public Exception getCause() {
        return cause;
    }
}
