package currency.util;

public class Timer {

    private long startTime;

    public Timer() {
        this.startTime = System.currentTimeMillis();
    }

    public void mark(String prefix) {
        System.out.println(prefix + ": " + (System.currentTimeMillis() - startTime) + " ms");
    }

}
