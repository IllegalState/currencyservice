package currency.service;

import com.sun.org.apache.xerces.internal.dom.ElementNSImpl;
import lb.client.*;
import lb.client.model.FxRateTypeHandling;
import currency.entity.Currency;
import currency.entity.CurrencyRate;
import currency.exception.ApplicationException;
import currency.parser.CurrencyListParser;
import currency.parser.CurrencyRateParser;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

public class CurrencyClientService {

    public static List<Currency> getCurrencyList() throws ApplicationException {
        try {
            FxRatesSoap endpoint = getEndpoint();
            GetCurrencyListResponse.GetCurrencyListResult currencyListResponse = endpoint.getCurrencyList();
            return parseCurrencyListResponse(currencyListResponse);
        } catch (ParserConfigurationException | SAXException | IOException | TransformerException e) {
            throw new ApplicationException(e);
        }
    }

    public static List<CurrencyRate> getCurrentFxRates() throws ApplicationException {
        return getCurrentFxRates(FxRateTypeHandling.EU);
    }

    public static List<CurrencyRate> getCurrentFxRates(FxRateTypeHandling rateType) throws ApplicationException {
        try {
            FxRatesSoap endpoint = getEndpoint();
            GetCurrentFxRatesResponse.GetCurrentFxRatesResult currentFxRatesResponse = endpoint.getCurrentFxRates(rateType.value());
            return parseCurrentFxRatesResponse(currentFxRatesResponse);
        } catch (ParserConfigurationException | SAXException | IOException | TransformerException e) {
            throw new ApplicationException(e);
        }
    }

    public static CurrencyRate getCurrencyRateClosestToDate(String currency, String date) throws ApplicationException {
        return getCurrencyRateClosestToDate(currency, date, FxRateTypeHandling.EU);
    }

    public static CurrencyRate getCurrencyRateClosestToDate(String currency, String date, FxRateTypeHandling rateType) throws ApplicationException {
        try {
            FxRatesSoap endpoint = getEndpoint();
            GetFxRatesForCurrencyResponse.GetFxRatesForCurrencyResult ratesForCurrencyResponse = endpoint.getFxRatesForCurrency(rateType.value(), currency, date, date);
            return parseRatesForCurrencyResponse(ratesForCurrencyResponse);
        } catch (ParserConfigurationException | SAXException | IOException | TransformerException e) {
            throw new ApplicationException(e);
        }
    }

    private static FxRatesSoap getEndpoint() {
        return new FxRates().getFxRatesSoap();
    }

    private static List<Currency> parseCurrencyListResponse(GetCurrencyListResponse.GetCurrencyListResult response) throws ParserConfigurationException, SAXException, IOException, TransformerException {
        InputStream is = responseToInputStream(response.getContent().get(0));
        CurrencyListParser currencyListParser = new CurrencyListParser();

        SAXParserFactory parserFactory = SAXParserFactory.newInstance();
        SAXParser parser = parserFactory.newSAXParser();
        parser.parse(is, currencyListParser);

        return currencyListParser.getCurrencies();
    }

    private static List<CurrencyRate> parseCurrentFxRatesResponse(GetCurrentFxRatesResponse.GetCurrentFxRatesResult response) throws TransformerException, ParserConfigurationException, SAXException, IOException {
        InputStream is = responseToInputStream(response.getContent().get(0));
        CurrencyRateParser currencyRateParser = new CurrencyRateParser();

        SAXParserFactory parserFactory = SAXParserFactory.newInstance();
        SAXParser parser = parserFactory.newSAXParser();
        parser.parse(is, currencyRateParser);

        return currencyRateParser.getRates();
    }

    private static CurrencyRate parseRatesForCurrencyResponse(GetFxRatesForCurrencyResponse.GetFxRatesForCurrencyResult response) throws ParserConfigurationException, SAXException, IOException, TransformerException {
        InputStream is = responseToInputStream(response.getContent().get(0));
        CurrencyRateParser currencyRateParser = new CurrencyRateParser();

        SAXParserFactory parserFactory = SAXParserFactory.newInstance();
        SAXParser parser = parserFactory.newSAXParser();
        parser.parse(is, currencyRateParser);

        return currencyRateParser.getRates().get(0);
    }

    private static InputStream responseToInputStream(Object node) throws TransformerException {
        Document doc = ((ElementNSImpl) node).getOwnerDocument();
        Source xmlSource = new DOMSource(doc);
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        Result outputTarget = new StreamResult(outputStream);
        TransformerFactory.newInstance().newTransformer().transform(xmlSource, outputTarget);
        return new ByteArrayInputStream(outputStream.toByteArray());
    }
}
