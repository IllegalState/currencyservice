package currency.parser;

import currency.entity.CurrencyRate;
import currency.parser.constants.CurrencyRateTags;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;

import java.util.ArrayList;
import java.util.List;

public class CurrencyRateParser extends ParserTemplate {

    private static final String BASE_CURRENCY_RATE = "1";

    private List<CurrencyRate> rates = new ArrayList<>();
    private CurrencyRate currentRate;

    private boolean isDate;
    private boolean isCurrencyAcronym;
    private boolean isRate;

    private String currencyAcronym;

    @Override
    public void startElement(String tagName, Attributes attributes) throws SAXException {
        String currentTag = tagName.toUpperCase();

        switch (CurrencyRateTags.valueOf(currentTag)) {
            case FXRATE:
                currentRate = new CurrencyRate();
                rates.add(currentRate);
                break;
            case DT:
                isDate = true;
                break;
            case CCY:
                isCurrencyAcronym = true;
                break;
            case AMT:
                isRate = true;
                break;
            default:
                break;
        }

    }

    @Override
    public void elementValue(String value) throws SAXException {
        if (isDate) {
            currentRate.setDate(value);
            isDate = false;
        }

        if (isCurrencyAcronym) {
            currencyAcronym = value;
            isCurrencyAcronym = false;
        }

        if (isRate) {

            if (BASE_CURRENCY_RATE.equals(value)) {
                currentRate.setFromCcy(currencyAcronym);
            } else {
                currentRate.setToCcy(currencyAcronym);
                currentRate.setRate(value);
            }

            isRate = false;
            currencyAcronym = null;
        }
    }

    public List<CurrencyRate> getRates() {
        return rates;
    }
}
