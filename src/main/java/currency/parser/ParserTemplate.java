package currency.parser;

import currency.parser.constants.ErrorTags;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public abstract class ParserTemplate extends DefaultHandler {

    private boolean error;
    private boolean errorDesc;

    @Override
    public final void startElement(String uri, String localName, String tagName, Attributes attributes) throws SAXException {
        String currentTag = tagName.toUpperCase();

        switch (ErrorTags.valueOf(currentTag)) {
            case OPRLERR:
                error = true;
                break;
            case DESC:
                if (error) {
                    errorDesc = true;
                }
                break;
            default:
                break;
        }

        if (!error) {
            startElement(tagName, attributes);
        }
    }

    @Override
    public final void characters(char[] ch, int start, int length) throws SAXException {
        String value = new String(ch, start, length);

        if (error && errorDesc) {
            throw new SAXException(value);
        }
        if (!error) {
            elementValue(value);
        }
    }

    protected abstract void startElement(String tagName, Attributes attributes) throws SAXException;

    protected abstract void elementValue(String value) throws SAXException;

}
