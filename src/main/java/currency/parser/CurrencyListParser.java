package currency.parser;

import currency.entity.Currency;
import currency.parser.constants.CurrencyListLanguages;
import currency.parser.constants.CurrencyListTags;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;

import java.util.ArrayList;
import java.util.List;

import static currency.parser.constants.CurrencyListLanguages.EN;
import static currency.parser.constants.CurrencyListLanguages.LT;

public class CurrencyListParser extends ParserTemplate {

    private List<Currency> currencies = new ArrayList<>();
    private Currency currentEntry;

    private boolean currencyName;
    private boolean currencyAcronym;

    private CurrencyListLanguages nameLanguage;

    @Override
    public void startElement(String tagName, Attributes attributes) throws SAXException {
        String currentTag = tagName.toUpperCase();

        switch (CurrencyListTags.valueOf(currentTag)) {
            case CCYNTRY:
                currentEntry = new Currency();
                currencies.add(currentEntry);
                break;
            case CCYNM:
                currencyName = true;
                String lang = attributes.getValue("lang");
                nameLanguage = CurrencyListLanguages.valueOf(lang);
                break;
            case CCY:
                currencyAcronym = true;
                break;
            default:
                break;
        }

    }

    @Override
    public void elementValue(String value) throws SAXException {
        if (currencyAcronym) {

            currentEntry.setAcronym(value);
            currencyAcronym = false;

        }

        if (currencyName) {

            if (EN.equals(nameLanguage)) {
                currentEntry.setNameEn(value);
            } else if (LT.equals(nameLanguage)) {
                currentEntry.setNameLt(value);
            }

            currencyName = false;
            nameLanguage = null;
        }

    }

    public List<Currency> getCurrencies() {
        return currencies;
    }
}
