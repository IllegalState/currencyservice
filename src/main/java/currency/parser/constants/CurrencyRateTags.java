package currency.parser.constants;

public enum CurrencyRateTags {

    FXRATE,
    DT,
    CCY,
    AMT

}
