package currency.entity;

public class Currency {

    private String nameLt;
    private String nameEn;
    private String acronym; //TODO CcyISO4217

    public String getNameLt() {
        return nameLt;
    }

    public void setNameLt(String nameLt) {
        this.nameLt = nameLt;
    }

    public String getNameEn() {
        return nameEn;
    }

    public void setNameEn(String nameEn) {
        this.nameEn = nameEn;
    }

    public String getAcronym() {
        return acronym;
    }

    public void setAcronym(String acronym) {
        this.acronym = acronym;
    }

    @Override
    public String toString() {
        return "{" + acronym + " / " + nameLt + " / " + nameEn + "}";
    }
}
