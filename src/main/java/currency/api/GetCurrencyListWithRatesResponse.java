package currency.api;

public class GetCurrencyListWithRatesResponse {

    private String ccy;
    private String nameLT;
    private String nameEN;
    private String rate;

    public GetCurrencyListWithRatesResponse(String ccy, String nameLT, String nameEN, String rate) {
        this.ccy = ccy;
        this.nameLT = nameLT;
        this.nameEN = nameEN;
        this.rate = rate;
    }

    public String getCcy() {
        return ccy;
    }

    public void setCcy(String ccy) {
        this.ccy = ccy;
    }

    public String getNameLT() {
        return nameLT;
    }

    public void setNameLT(String nameLT) {
        this.nameLT = nameLT;
    }

    public String getNameEN() {
        return nameEN;
    }

    public void setNameEN(String nameEN) {
        this.nameEN = nameEN;
    }

    public String getRate() {
        return rate;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }
}
