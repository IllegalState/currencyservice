package currency.api;

import currency.entity.Currency;
import currency.entity.CurrencyRate;
import currency.service.CurrencyClientService;
import currency.util.Timer;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collector;
import java.util.stream.Collectors;

@RestController
public class GetCurrencyListWithRates {

    private static final Collector<Currency, ?, Map<String, Currency>> TO_MAP = Collectors.toMap(Currency::getAcronym, Function.identity());

    @RequestMapping("/currencyListWithRates")
    public List<GetCurrencyListWithRatesResponse> get() {
        Timer timer = new Timer();

        List<Currency> currencyList = CurrencyClientService.getCurrencyList();
        List<CurrencyRate> currencyRates = CurrencyClientService.getCurrentFxRates();
        timer.mark("Time taken to get info");

        Map<String, Currency> currencyMap = currencyList.stream().collect(TO_MAP);

        List<GetCurrencyListWithRatesResponse> response = currencyRates.stream()
                .filter(r -> currencyMap.containsKey(r.getToCcy()))
                .map(toResponse(currencyMap))
                .collect(Collectors.toList());
        timer.mark("Time taken total");
        return response;
    }

    private Function<CurrencyRate, GetCurrencyListWithRatesResponse> toResponse(Map<String, Currency> currencyMap) {
        return r -> {
            Currency currency = currencyMap.get(r.getToCcy());

            String acronym = currency.getAcronym();
            String nameLt = currency.getNameLt();
            String nameEn = currency.getNameEn();
            String rate = r.getRate();

            return new GetCurrencyListWithRatesResponse(acronym, nameLt, nameEn, rate);
        };
    }
}
