package currency.api;

import currency.entity.CurrencyRate;
import currency.service.CurrencyClientService;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class GetCurrencyRateClosestToDate {

    @RequestMapping(value = "/currencyRate/{ccy}/{date}", method = RequestMethod.GET)
    public GetCurrencyRateClosestToDateResponse get(@PathVariable("ccy")String currency, @PathVariable("date")String date) {
        CurrencyRate rate = CurrencyClientService.getCurrencyRateClosestToDate(currency, date);
        return rateToResponse(rate);
    }

    private GetCurrencyRateClosestToDateResponse rateToResponse(CurrencyRate currencyRate) {
        GetCurrencyRateClosestToDateResponse response = new GetCurrencyRateClosestToDateResponse();

        response.setCcy(currencyRate.getToCcy());
        response.setDate(currencyRate.getDate());
        response.setRate(currencyRate.getRate());

        return response;
    }
}
